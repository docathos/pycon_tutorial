# README #

### Requirements ###

* The tutorial will be using python 2.7. Alternatively, Kivy mostly works with Python 3. Mostly.
* You will need Jupyter Notebook and Kivy, as well as the notebooks and example data contained in this repo.
* Easiest way to install Jupyter Notebook is by using [Anaconda](https://www.anaconda.com/download/)
* Easiest / only way to install Kivy is by using the instructions at the [Kivy homepage](https://kivy.org/#download)
* Note that we are not going to be running Kivy in the notebook, just from the command line.
* (Optional) We will show how to build Kivy apps for iOS. To run this yourself requires the [kivy-ios](https://github.com/kivy/kivy-ios) package.

### Who do I talk to? ###

* Steve Astels, sastels@gmail.com
